package com.ftninformatika.jwd.olimp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.olimp.model.Takmicar;

@Repository
public interface TakmicarRepository extends JpaRepository<Takmicar, Long> {
	
	Takmicar findOneById(Long id);
	
	Page<Takmicar> findByDrzavaIdAndBrojMedaljaBetween(Long drzavaId, Integer medaljaOd, Integer medaljaDo, Pageable p);
	Page<Takmicar> findByBrojMedaljaBetween(Integer medaljaOd, Integer medaljaDo, Pageable p);

}
