package com.ftninformatika.jwd.olimp.service;

import java.util.List;

import com.ftninformatika.jwd.olimp.model.Prijava;

public interface PrijavaService {
	
	Prijava findOneById(Long id);
	
	List<Prijava> findAll();
	
	Prijava save(Prijava p);
}
