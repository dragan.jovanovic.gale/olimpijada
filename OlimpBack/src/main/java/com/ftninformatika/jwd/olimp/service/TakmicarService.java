package com.ftninformatika.jwd.olimp.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.olimp.model.Takmicar;

public interface TakmicarService {
	
	Takmicar findOneById(Long id);
	
	List<Takmicar> findAll();
	
	Takmicar save(Takmicar t);
	
	Takmicar update(Takmicar t);
	
	Takmicar delete(Long id);
	
	Page <Takmicar> find(Long drzavaId,Integer medaljeOd, Integer medaljeDo,Integer pageNo);
}
